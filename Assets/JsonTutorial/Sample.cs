using Newtonsoft.Json;
using UnityEngine;

public sealed class Sample : MonoBehaviour
{
    public void Awake()
    {
        var data = new SampleData()
        {
            AAA = @"GGG\nGG",
            BBB = 123
        };

        var sampleStr = $"AAA = {data.AAA}";
        var json = JsonConvert.SerializeObject(data);
        var anotherData = JsonConvert.DeserializeObject<SampleData>(json);
        Debug.Log(anotherData.AAA);
    }
}
