using UnityEngine;
using ittimstudio;

public sealed class EEJSampleMain : MonoBehaviour
{
    private EEJSheet table1;
    private EEJSheet table2;

    private void Start()
    {
        table1 = EEJSheet.LoadFromResources(@"EEJson/Json/Sample1");
        table2 = EEJSheet.LoadFromResources(@"EEJson/Json/Sample2");

        Debug.Log(table1.Contents[0][0]);
        Debug.Log(table2.Contents[1][1]);
        Debug.Log(table1.SheetName);
    }
}