namespace ittimstudio
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class EEJSheet
    {
        [JsonProperty]
        public string SheetName { get; private set; }

        [JsonProperty]
        public Dictionary<int, Dictionary<int, string>> Contents = new Dictionary<int, Dictionary<int, string>>();

        public EEJSheet(string name)
        {
            SheetName = name;
        }

        public EEJSheet()
        {
        }

        public static EEJSheet LoadFromResources(string filePath)
        {
            var text = Resources.Load(filePath).ToString();
            if (string.IsNullOrEmpty(text))
            {
                return new EEJSheet(string.Empty);
            }

            var sheet = JsonConvert.DeserializeObject<EEJSheet>(text);
            return sheet;
        }
    }
}