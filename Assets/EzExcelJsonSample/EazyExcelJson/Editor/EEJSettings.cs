namespace ittimstudioEditor
{
    using System;

    [Serializable]
    internal sealed class EEJSettings
    {
        public const string SETTINGS_PATH = @"ProjectSettings/EEJSettings.json";

        public string SourcePathRoot;

        public string OutputPathRoot;
    }
}