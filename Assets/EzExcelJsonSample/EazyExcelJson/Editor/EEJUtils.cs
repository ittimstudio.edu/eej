using System;
using System.Collections.Generic;
using System.Linq;
using ittimstudio;

namespace ittimstudioEditor
{
    using UnityEditor;
    using System.Text;
    using System.IO;
    using Newtonsoft.Json;
    using static EEJSettings;

    internal static class EEJUtils
    {
        #region API

        public static EEJSettings LoadSettings()
            => LoadJson<EEJSettings>(SETTINGS_PATH);

        public static void SaveSettings(EEJSettings settings)
            => SaveJson(settings, SETTINGS_PATH);

        public static void GenerateSheetJson(EEJSettings settings)
        {
            if (settings == default || !Directory.Exists(settings.SourcePathRoot))
            {
                return;
            }

            var sheets = LoadAllCsv(settings);
            foreach (var sheet in sheets)
            {
                SaveSheet(settings, sheet);
            }

            AssetDatabase.Refresh();

            void SaveSheet(EEJSettings settings, EEJSheet sheet)
            {
                var outputPathFolder = Path.Combine(settings.OutputPathRoot, @"Json");
                var outputPath = $"{outputPathFolder}/{sheet.SheetName}.json";
                var json = JsonConvert.SerializeObject(sheet, Formatting.Indented);
                CreateFolderSafe(outputPath);
                File.WriteAllText(outputPath, json, Encoding.UTF8);
            }
        }

        #endregion

        #region private method

        private static T LoadJson<T>(string filePath)
            where T : class, new()
        {
            if (!File.Exists(SETTINGS_PATH))
            {
                return new T();
            }

            var json = File.ReadAllText(SETTINGS_PATH, Encoding.UTF8);
            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        private static void SaveJson<T>(T content, string filePath)
            where T : class, new()
        {
            if (content == default)
            {
                content = new T();
            }

            CreateFolderSafe(filePath);

            var json = JsonConvert.SerializeObject(content, Formatting.Indented);
            File.WriteAllText(filePath, json, Encoding.UTF8);
        }

        private static void CreateFolderSafe(string filePath)
        {
            var path = new FileInfo(filePath);
            if (path.Directory is { Exists: true })
            {
                return;
            }

            path.Directory.Create();
            AssetDatabase.Refresh();
        }

        private static EEJSheet[] LoadAllCsv(EEJSettings settings)
        {
            if (!Directory.Exists(settings.SourcePathRoot))
            {
                return Array.Empty<EEJSheet>();
            }

            var files = Directory.GetFiles(settings.SourcePathRoot, @"*.csv", SearchOption.AllDirectories);
            var result = files.Select(LoadCsv).ToArray();
            return result;
        }

        private static EEJSheet LoadCsv(string filePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var sheet = new EEJSheet(fileName);

            if (!File.Exists(filePath))
            {
                return sheet;
            }

            var lines = File.ReadAllLines(filePath, Encoding.UTF8);
            var sep = ',';
            for (var row = 0; row < lines.Length; row++)
            {
                var cells = lines[row].Split(sep);

                for (var cell = 0; cell < cells.Length; cell++)
                {
                    var value = cells[cell];
                    if (!sheet.Contents.ContainsKey(row))
                    {
                        sheet.Contents[row] = new Dictionary<int, string>();
                    }

                    sheet.Contents[row][cell] = value;
                }
            }

            return sheet;
        }

        #endregion
    }
}