namespace ittimstudioEditor
{
    using UnityEditor;
    using UnityEngine;
    using static EEJUtils;
    using static EEJSettings;

    internal sealed class EEJSettingsWindow : EditorWindow
    {
        private EEJSettings _settings;

        [MenuItem(@"Tools/EEJ/Settings")]
        private static void Open()
        {
            GetWindow<EEJSettingsWindow>(@"EEJSettings");
        }

        [MenuItem(@"Tools/EEJ/RevealInFinder")]
        private static void RevealInFinder()
        {
            EditorUtility.RevealInFinder(SETTINGS_PATH);
        }

        private void OnEnable()
        {
            _settings = LoadSettings();
        }

        private void OnGUI()
        {
            _settings.SourcePathRoot =
                EditorGUILayout.TextField(nameof(_settings.SourcePathRoot), _settings.SourcePathRoot);
            _settings.OutputPathRoot =
                EditorGUILayout.TextField(nameof(_settings.OutputPathRoot), _settings.OutputPathRoot);

            GUILayout.FlexibleSpace();

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button(@"Convert"))
                {
                    GenerateSheetJson(_settings);
                    Close();
                }
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button(@"Revert", EditorStyles.toolbarButton))
                {
                    LoadSettings();
                    Close();
                }

                if (GUILayout.Button(@"Apply", EditorStyles.toolbarButton))
                {
                    SaveSettings(_settings);
                    Close();
                }
            }
        }
    }
}