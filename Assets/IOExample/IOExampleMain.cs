using System;
using System.Collections;
using System.Collections.Generic;
using ittimstudio;
using UnityEngine;

public class IOExampleMain : MonoBehaviour
{
    private enum DataSource
    {
        PLAYER_PREFS,
        DEVICE_PREFS
    }
    
    private string[] keys = new []
    {
        @"CurHP",
        @"HPMax"
    };

    [SerializeField]
    private DataSource _dataSourceType = DataSource.PLAYER_PREFS;

    private int _curHp
    {
        get => GetInt(keys[0], _curHpMax);
        set => SetInt(keys[0], value);
    }

    private int _curHpMax
    {
        get => GetInt(keys[1], 100);
        set => SetInt(keys[1], value);
    }

    private void OnGUI()
    {
        GUILayout.Label($"HP : {_curHp} / {_curHpMax}");
        if (GUILayout.Button(@"Attack"))
        {
            _curHp = Mathf.Max(0, _curHp - 10);
        }

        if (GUILayout.Button(@"Reset"))
        {
            _curHp = _curHpMax;
        }
    }

    private int GetInt(string key, int defaultValue)
    {
        return _dataSourceType switch
        {
            DataSource.PLAYER_PREFS => PlayerPrefs.GetInt(key, defaultValue),
            DataSource.DEVICE_PREFS => DevicePrefs.GetInt(key, defaultValue),
            _ => defaultValue
        };
    }

    private void SetInt(string key, int value)
    {
        switch (_dataSourceType)
        {
            case DataSource.PLAYER_PREFS:
                PlayerPrefs.SetInt(key, value);
                return;
            case DataSource.DEVICE_PREFS:
                DevicePrefs.SetInt(key, value);
                return;
            default:
                return;
        }
    }
}
