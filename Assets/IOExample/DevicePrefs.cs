namespace ittimstudio
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;
    using UnityEngine.Assertions;

    public static class DevicePrefs
    {
        #region sub type
        [Serializable]
        private sealed class Data
        {
            public Dictionary<string, string> StrDic = new Dictionary<string, string>();
            public Dictionary<string, int> IntDic = new Dictionary<string, int>();
            public Dictionary<string, float> FloatDic = new Dictionary<string, float>();
        }
        #endregion

        #region variable
        /// <summary>
        /// Dispatch error message inside DevicePrefs
        /// </summary>
        public static event Action<Exception> OnErrorEvent;
        private static string _FilePath { get; } = $"{Application.temporaryCachePath}/device.prefs";
        private static Data _data;
        #endregion

        #region API
        static DevicePrefs()
        {
            if (!File.Exists(_FilePath))
            {
                Reset();
                return;
            }

            try
            {
                var json = File.ReadAllText(_FilePath);
                _data = JsonConvert.DeserializeObject<Data>(json);
            }
            catch
            {
                // Auto migration if Data class has been changed
                Reset();
            }

            void Reset()
            {
                _data = new Data();
                Save();
                Debug.Log(@"DevicePrefs has been reset");
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        public static void Save()
        {
            Assert.IsNotNull(_data);

            try
            {
                var json = JsonConvert.SerializeObject(_data);
                File.WriteAllText(_FilePath, json);
            }
            catch(Exception e)
            {
                OnErrorEvent?.Invoke(e);
            }
        }

        /// <summary>
        /// Get string value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetString(string key, string defaultValue)
        {
            if (_data.StrDic.TryGetValue(key, out var result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// Set string value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetString(string key, string value)
        {
            _data.StrDic[key] = value;
            Save();
        }

        /// <summary>
        /// Get int value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetInt(string key, int defaultValue)
        {
            if (_data.IntDic.TryGetValue(key, out var result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// Set int value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetInt(string key, int value)
        {
            _data.IntDic[key] = value;
            Save();
        }

        /// <summary>
        /// Get float value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        public static float GetFloat(string key, float defaultValue)
        {
            if (_data.FloatDic.TryGetValue(key, out var result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// Set float value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetFloat(string key, float value)
        {
            _data.FloatDic[key] = value;
            Save();
        }

        /// <summary>
        /// Has key check
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool HasKey(string key)
        {
            var result =
                _data.StrDic.ContainsKey(key) ||
                _data.IntDic.ContainsKey(key) ||
                _data.FloatDic.ContainsKey(key);
            return result;
        }

        /// <summary>
        /// Delete key
        /// </summary>
        /// <param name="key"></param>
        public static void DeleteKey(string key)
        {
            if (_data.StrDic.ContainsKey(key))
            {
                _data.StrDic.Remove(key);
            }

            if (_data.IntDic.ContainsKey(key))
            {
                _data.IntDic.Remove(key);
            }

            if (_data.FloatDic.ContainsKey(key))
            {
                _data.FloatDic.Remove(key);
            }

            Save();
        }

        /// <summary>
        /// Delete all keys
        /// </summary>
        public static void DeleteAll()
        {
            _data.StrDic.Clear();
            _data.IntDic.Clear();
            _data.FloatDic.Clear();
            Save();
        }
        #endregion
    }
}
